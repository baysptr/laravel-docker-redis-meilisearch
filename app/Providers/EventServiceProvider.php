<?php

namespace App\Providers;

use App\Events\RecipeProcessed;
use App\Listeners\UpdateProductNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use function Illuminate\Events\queueable;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

//        RecipeProcessed::class => [
//            UpdateProductNotification::class
//        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
//        Event::listen(queueable(function (RecipeProcessed $event){
//            $event->broadcastOn([UpdateProductNotification::class, 'handle']);
//        })->onConnection('redis')->onQueue('recipies')->delay(now()->addSeconds(3)));
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
