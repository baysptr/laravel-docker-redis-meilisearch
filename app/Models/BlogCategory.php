<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class BlogCategory extends Model
{
    use HasFactory, LogsActivity, Searchable;

    protected $fillable = ['category'];

    public function searchableAs()
    {
        return 'blog_category_index';
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty()
            ->useLogName('blog_category')
            ->dontSubmitEmptyLogs();
    }

    public function blog_post(){
        return $this->hasMany(BlogPost::class, 'blog_category_id', 'id');
    }
}
