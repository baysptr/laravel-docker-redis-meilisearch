<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecipeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recipe;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recipe)
    {
        $this->recipe = $recipe;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->delay(3);

        $product = Product::find($this->recipe->product_id);
        $recipe = Recipe::find($this->recipe->id);

        $decrement_stock_product = $product->stock - $recipe->quantity;

        $product->update(['stock'=>$decrement_stock_product]);
        $recipe->update(['status'=>true]);
    }
}
