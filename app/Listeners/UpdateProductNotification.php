<?php

namespace App\Listeners;

use App\Events\RecipeProcessed;
use App\Models\Product;
use App\Models\Recipe;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateProductNotification implements ShouldQueue
{
    use InteractsWithQueue;

    public $connection = 'redis';
    public $queue = 'recipies';
    public $delay = 5;
    public $tries = 5;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\RecipeProcessed  $event
     * @return void
     */
    public function handle(RecipeProcessed $event)
    {
        $product = Product::find($event->recipe->product_id);
        $recipe = Recipe::find($event->recipe->id);

        $decrement_stock_product = $product->stock - $recipe->quantity;

        $product->update(['stock'=>$decrement_stock_product]);
        $recipe->update(['status'=>true]);
    }
}
