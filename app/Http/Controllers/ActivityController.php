<?php

namespace App\Http\Controllers;

use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    public function activity(){
        $data = Activity::select('log_name', 'description', 'properties', 'created_at', 'event')
            ->causedBy(auth()->user())->paginate(10);

        return response()->json(['data'=>$data]);
    }
}
