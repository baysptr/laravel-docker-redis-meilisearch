<?php

namespace App\Http\Controllers;

use App\Events\RecipeProcessed;
use App\Jobs\RecipeJob;
use App\Models\Product;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Recipe::search(query: trim($request->search) ?? '')->paginate(10);

        return response()->json([
            'code'=>Response::HTTP_OK,
            'message'=>'SUCCESS',
            'data'=>$data],
            200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'product_id'=>'required|exists:products,id',
            'quantity'=>'required|numeric',
            'pay'=>'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }else{
            $product = Product::find($request->product_id);
            $product_stock = $product->stock;
            $product_price = $product->price;
            $product_accumulative = ($request->quantity * $product_price);

            if ($request->quantity > $product_stock){
                return response()->json([
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => "Maaf pembelian melebihi stock yang tersedia"
                ]);
            }else if($request->pay < $product_accumulative){
                return response()->json([
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => "Maaf jumlah pembayaran tidak sesuai jumlah harga product"
                ]);
            }else{
                DB::beginTransaction();
                try {
                    $recipe = Recipe::create($validator->validated());

                    DB::commit();

//                    Event::dispatch(new RecipeProcessed($recipe));
                    RecipeJob::dispatch($recipe);

                    return response()->json([
                        'code' => Response::HTTP_CREATED,
                        'message' => 'INSERTED',
                        'data'=>$recipe
                    ], Response::HTTP_CREATED);
                }catch (\Exception $e){
                    DB::rollback();

                    return response()->json([
                        'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                        'message' => $e->getMessage(),
                    ]);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recipe  $Recipe
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Recipe::query()->find($id);

        if ($data){
            return response()->json([
                'code'=>Response::HTTP_OK,
                'message'=>'SUCCESS',
                'data'=>$data],
                200);
        }else{
            return response()->json([
                'code'=>Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'=>'Data is not found'],
                200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recipe  $Recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'product_id'=>'required|exists,products',
            'quantity'=>'required|numeric',
            'pay'=>'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try {
            Recipe::query()->findOrFail($id)->update($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'UPDATED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recipe  $Recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        DB::beginTransaction();
        try{
            Recipe::query()->findOrFail($id)->delete();

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'DELETED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);

        }
    }
}
