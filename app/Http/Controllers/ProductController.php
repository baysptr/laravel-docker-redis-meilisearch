<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Product::search(query: trim($request->search) ?? '')->paginate(10);

        return response()->json([
            'code'=>Response::HTTP_OK,
            'message'=>'SUCCESS',
            'data'=>$data],
            200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'price'=>'required|numeric',
            'stock'=>'required|numeric',
            'description'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try {
            Product::create($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_CREATED,
                'message' => 'INSERTED',
            ], Response::HTTP_CREATED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::query()->find($id);

        if ($data){
            return response()->json([
                'code'=>Response::HTTP_OK,
                'message'=>'SUCCESS',
                'data'=>$data],
                200);
        }else{
            return response()->json([
                'code'=>Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'=>'Data is not found'],
                200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'=>'required',
            'price'=>'required|numeric',
            'stock'=>'required|numeric',
            'description'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try {
            Product::query()->findOrFail($id)->update($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'UPDATED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        DB::beginTransaction();
        try{
            Product::query()->findOrFail($id)->delete();

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'DELETED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);

        }
    }
}
