<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = BlogPost::search(query: trim($request->search) ?? '')->paginate(10);

        return response()->json([
            'code'=>Response::HTTP_OK,
            'message'=>'SUCCESS',
            'data'=>$data],
            200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'blog_category_id' => 'required|numeric|exists:blog_categories,id',
            'blog_tag_id' => 'required|numeric|exists:blog_tags,id',
            'title'=>'required',
            'deskripsi'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try {
            BlogPost::create($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_CREATED,
                'message' => 'INSERTED',
            ], Response::HTTP_CREATED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogPost  $BlogPost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = BlogPost::query()->with(['blog_tag', 'blog_category'])->find($id);

        if ($data){
            return response()->json([
                'code'=>Response::HTTP_OK,
                'message'=>'SUCCESS',
                'data'=>$data],
                200);
        }else{
            return response()->json([
                'code'=>Response::HTTP_INTERNAL_SERVER_ERROR,
                'message'=>'Data is not found'],
                200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogPost  $BlogPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'blog_category_id' => 'required|numeric|exists:blog_categories,id',
            'blog_tag_id' => 'required|numeric|exists:blog_tags,id',
            'title'=>'required',
            'deskripsi'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try {
            BlogPost::query()->findOrFail($id)->update($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'UPDATED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogPost  $BlogPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        DB::beginTransaction();
        try{
            BlogPost::query()->findOrFail($id)->delete();

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'DELETED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);

        }
    }
}
