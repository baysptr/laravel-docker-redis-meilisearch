<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BlogPost>
 */
class BlogPostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'blog_category_id' => fake()->numberBetween(1, 20),
            'blog_tag_id' => fake()->numberBetween(1, 20),
            'title' => fake()->realText('100'),
            'deskripsi' => fake()->paragraph('300')
        ];
    }
}
