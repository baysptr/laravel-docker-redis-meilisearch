<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            User::create([
                'name'=>'Admin',
                'username'=>'admin',
                'email'=>'admin@alba.tech',
                'password'=>bcrypt('password')
            ]);

            DB::commit();
        }catch (\Exception $exception){
            Log::info($exception);
            DB::rollBack();
        }
    }
}
