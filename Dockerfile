FROM php:8.1-fpm-alpine

RUN apk add --no-cache ${PHPIZE_DEPS} libpq-dev oniguruma-dev shadow icu-dev zlib-dev libpng-dev
RUN apk del --no-cache ${PHPIZE_DEPS}
RUN docker-php-ext-install pdo pdo_pgsql pgsql sockets bcmath mbstring intl gd
RUN apk add --no-cache pcre-dev $PHPIZE_DEPS \
    && pecl install redis \
    && docker-php-ext-enable redis.so
RUN curl -sS https://getcomposer.org/installer​ | php -- \
     --install-dir=/usr/local/bin --filename=composer

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /var/www
COPY . .
RUN composer install --no-interaction
RUN php artisan key:generate
RUN php artisan jwt:secret

EXPOSE 9000

CMD ["php-fpm"]
