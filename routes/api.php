<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogTagController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\BlogCategoryController;
use App\Http\Controllers\BlogPostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RecipeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:api')->group(function (){
    Route::controller(ActivityController::class)
        ->prefix('activity')
        ->group(function (){
            Route::get('/', 'activity');
        });

    Route::controller(AuthController::class)
        ->group(function (){
            Route::post('logout', 'logout');
            Route::post('refresh', 'refresh');
            Route::post('me', 'me');
        });

    Route::controller(BlogTagController::class)
        ->prefix('blog/tag')
        ->group(function (){
           Route::get('/', 'index');
           Route::get('/show/{id}', 'show');
           Route::post('/store', 'store');
           Route::post('/update/{id}', 'update');
           Route::post('/delete', 'destroy');
        });

    Route::controller(BlogCategoryController::class)
        ->prefix('blog/category')
        ->group(function (){
            Route::get('/', 'index');
            Route::get('/show/{id}', 'show');
            Route::post('/store', 'store');
            Route::post('/update/{id}', 'update');
            Route::post('/delete', 'destroy');
        });

    Route::controller(BlogPostController::class)
        ->prefix('blog/post')
        ->group(function (){
            Route::get('/', 'index');
            Route::get('/show/{id}', 'show');
            Route::post('/store', 'store');
            Route::post('/update/{id}', 'update');
            Route::post('/delete', 'destroy');
        });

    Route::controller(ProductController::class)
        ->prefix('product')
        ->group(function (){
            Route::get('/', 'index');
            Route::get('/show/{id}', 'show');
            Route::post('/store', 'store');
            Route::post('/update/{id}', 'update');
            Route::post('/delete', 'destroy');
        });

    Route::controller(RecipeController::class)
        ->prefix('buy')
        ->group(function (){
            Route::get('/', 'index');
            Route::get('/show/{id}', 'show');
            Route::post('/store', 'store');
            Route::post('/update/{id}', 'update');
            Route::post('/delete', 'destroy');
        });
});
